# L2JMobius - 天堂2开源中文汉化版 - 血玫瑰社区 - 杀气
[![Website www.l2fater.cn](https://img.shields.io/website-up-down-green-red/https/www.l2fater.cn.svg)](https://www.l2fater.cn/)

## 简介
L2JMobius 官网地址 https://l2jmobius.org/

本项目为不修改内核下，仅汉化html/scripts/configs等对话

###########部分汉化内容结构解析    
目录 dist\game

```
├── config                   // 各种游戏设置配置
├── data                      // 配置
│   ├── html                    // 各种对话（GM，商店，传送等）
│   ├── scripts               // 脚本（任务,副本,活动,职业等）
```


## 捐助
如果您觉得我们的项目对您有所帮助，请扫下方二维码打赏我们一杯蜜雪冰城甜蜜蜜。

![微信赞赏](https://foruda.gitee.com/images/1699409234265601529/fdd0eed0_1858155.jpeg "感谢您的赞赏")

## 联系
网站：
[https://www.l2fater.cn](https://www.l2fater.cn)(专注于天堂2单机玩家游戏体验的交流社区)

[https://www.17danji.cn](https://www.17danji.cn)（资源站，集成游戏，办公，娱乐等资源收录）

[https://17danji.cn](https://17danji.cn)(杀气丶自测，自编译网单-手游-页游-经验分享等)

邮箱:
51605539@qq.com
