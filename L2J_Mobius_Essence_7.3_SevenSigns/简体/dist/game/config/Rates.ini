# ---------------------------------------------------------------------------
# Rate Settings
# ---------------------------------------------------------------------------
# The defaults are set to be retail-like. If you modify any of these settings your server will deviate from being retail-like.
# Warning: 
# Please take extreme caution when changing anything. Also please understand what you are changing before you do so on a live server.

# ---------------------------------------------------------------------------
# Standard Settings (Retail value = 1)
# ---------------------------------------------------------------------------

# Experience multiplier
RateXp = 1
# Skill points multiplier
RateSp = 1
# Experience multiplier (Party)
RatePartyXp = 1
# Skill points multiplier (Party)
RatePartySp = 1

# Instance rates
# Those rates are used as absolute rate within instances, does not applies on top of RateXp for example!
# 默认: -1 (Uses the rates above)
# Instance Experience multiplier
RateInstanceXp = -1
# Instance Skill points multiplier
RateInstanceSp = -1
# Instance Experience multiplier (Party)
RateInstancePartyXp = -1
# Instance Skill points multiplier (Party)
RateInstancePartySp = -1

RateDropManor = 1
# Karma decreasing rate
# Note: -1 means RateXp so it means it will use retail rate for decreasing karma upon death or receiving exp by farming mobs.
# 默认: -1
RateKarmaLost = -1
RateKarmaExpLost = 1
RateSiegeGuardsPrice = 1

# Raidboss points multipler
RateRaidbossPointsReward = 1

# Modify the rate of reward of all extractable items and skills.
# 默认: 1.
RateExtractable = 1.

# Quest Multipliers
# Warning: Many quests need to be rewritten 
# for this setting to work properly.

# Quest item drop multiplier
RateQuestDrop = 1

# Exp/SP reward multipliers
RateQuestRewardXP = 1
RateQuestRewardSP = 1

# Adena reward multiplier
RateQuestRewardAdena = 1

# Use additional item multipliers?
# 默认: False
UseQuestRewardMultipliers = False

# 默认 reward multiplier
# When UseRewardMultipliers=False - default multiplier is used for any reward
# When UseRewardMultipliers=True  - default multiplier is used for all items not affected by additional multipliers
# 默认: 1
RateQuestReward = 1

# Additional quest-reward multipliers based on item type
RateQuestRewardPotion = 1
RateQuestRewardScroll = 1
RateQuestRewardRecipe = 1
RateQuestRewardMaterial = 1


# ---------------------------------------------------------------------------
# Item Drop Rates
# ---------------------------------------------------------------------------
# Remember if you increase both chance and amount you will have higher rates than expected.
# Example: if amount multiplier is 5 and chance multiplier is 5 you will end up with 5*5 = 25 drop rates so be careful!

# Multiplies the amount of items rewarded from monsters when they die.
DeathDropAmountMultiplier = 1
# Multiplies the amount of items rewarded from monsters when a Spoil skill is used.
SpoilDropAmountMultiplier = 1
# Multiplies the amount of items rewarded from monsters when they die.
HerbDropAmountMultiplier = 1
RaidDropAmountMultiplier = 1

# Multiplies the chance of items that can be rewarded from monsters when they die.
DeathDropChanceMultiplier = 1
# Multiplies the chance of items that can be rewarded from monsters when a Spoil skill is used.
SpoilDropChanceMultiplier = 1
# Multiplies the chance of items that can be rewarded from monsters when they die.
HerbDropChanceMultiplier = 1
RaidDropChanceMultiplier = 1

# List of items affected by custom drop rate by id, used now for Adena rate too.
# Usage: itemId1,multiplier1;itemId2,multiplier2;...
# Note: Make sure the lists do NOT CONTAIN trailing spaces or spaces between the numbers!
# Example for Raid boss 1x jewelry: 6656,1;6657,1;6658,1;6659,1;6660,1;6661,1;6662,1;8191,1;10170,1;10314,1;
# 默认: 57,1
DropAmountMultiplierByItemId = 57,1
DropChanceMultiplierByItemId = 57,1

# Maximum drop occurrences.
# Note: Items that have 100% drop chance without server rate multipliers
# are not counted by this value. They will drop as extra drops.
# Also grouped drops with total chance over 100% break this configuration.
DropMaxOccurrencesNormal = 2
DropMaxOccurrencesRaidboss = 7


# ---------------------------------------------------------------------------
# Item Drop Level Difference Settings
# ---------------------------------------------------------------------------

# Maximum level difference between a player and a monster where the monster can still drop Adena.
# 默认: 14
DropAdenaMaxLevelLowestDifference = 14

# Maximum level difference between a player and a monster where the monster can still drop items.
# 默认: 14
DropItemMaxLevelLowestDifference = 14

# Maximum level difference between a player and a monster where the monster can still drop event items.
# 默认: 14
EventItemMaxLevelLowestDifference = 14


# ---------------------------------------------------------------------------
# Vitality system rates. Works only if EnableVitality = True
# ---------------------------------------------------------------------------

# The following configures the XP multiplier of each vitality level. Basically, you have
# 默认: 3
# Take care setting these values according to your server rates, as the can lead to huge differences!
# Example with a server rate 15x and vitality = 2. => final server rate = 30 (15x2)!
RateVitalityExpMultiplier = 3
RateLimitedSayhaGraceExpMultiplier = 2

# Maximum vitality items allowed to be used for a week by a player.
# 默认: 0 - unlimited
VitalityMaxItemsAllowed = 0

# These options are to be used if you want to increase the vitality gain/lost for each mob you kills
# 默认 values are 1.
RateVitalityGain = 1.
RateVitalityLost = 1.

# ---------------------------------------------------------------------------
# Player Drops (values are set in PERCENTS)
# ---------------------------------------------------------------------------

PlayerDropLimit = 3
# in %
PlayerRateDrop = 5
# in %
PlayerRateDropItem = 70
# in %
PlayerRateDropEquip = 25
# in %
PlayerRateDropEquipWeapon = 5

# 默认: 10
KarmaDropLimit = 10

# 默认: 40
KarmaRateDrop = 40

# 默认: 50
KarmaRateDropItem = 50

# 默认: 40
KarmaRateDropEquip = 40

# 默认: 10
KarmaRateDropEquipWeapon = 10


# ---------------------------------------------------------------------------
# Pets (Default value = 1)
# ---------------------------------------------------------------------------

PetXpRate = 1
PetFoodRate = 1
SinEaterXpRate = 1


# ---------------------------------------------------------------------------
# Blessed items
# ---------------------------------------------------------------------------
# Chance of blessing weapon.
# 默认: 15.0
BlessingChance = 15.0


# ---------------------------------------------------------------------------
# Additional drops from raid bosses (except GrandBoss)
# ---------------------------------------------------------------------------

BossDropEnable = False
BossDropMinLevel = 40
BossDropMaxLevel = 999

# The following configures the items you want to add to the drop
# Usage: itemId1,minAmount1;maxAmount1,chance1;itemId2...
# 默认: L-Coin Pouch, min: 1x, max: 2x, 100% chance of drop
# PLEASE NOTE: Chance of drop also increases from VIP level/runes/etc.
BossDropList = 71856,1,2,100;


# ---------------------------------------------------------------------------
# LCoin Drop
# ---------------------------------------------------------------------------

# LCoin drops from mobs.
# 默认: False
LCoinDropEnable = False

# LCoin drop chance from normal mobs.
# 默认: 15.0
LCoinDropChance = 15.0

# LCoin drop minimum monster level.
# 默认: 40
LCoinMinimumMonsterLevel = 40

# LCoin drop minimum drop quantity.
# 默认: 1
LCoinMinDropQuantity = 1

# LCoin drop max drop quantity.
# 默认: 5
LCoinMaxDropQuantity= 5

